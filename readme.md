## Before continuing:
**Note:** **This proyect** refers to this [other project](https://gitlab.com/lucasBear/jwit-react_basico-2)
# React 4 - Refactor TypeScript

This project was created following the course from Jwit platform.

[![react4-refactor-TS.png](https://i.postimg.cc/4x6RP3yT/react4-refactor-TS.png)](https://postimg.cc/4Yx0Nsp8)

# React 5 - Refactor 2 TypeScript

This project was created following the course from Jwit platform.

[![react5-refactor-TS.png](https://i.postimg.cc/65yfKt3v/react5-refactor-TS.png)](https://postimg.cc/JGCkcwjr)
## Available Scripts

In the project directory, you can run:

### `npm install`

Install all the dependences.

### `npm run dev`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.