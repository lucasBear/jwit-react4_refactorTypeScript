import styled from "styled-components";
import Menu from "./Menu";

const Container = styled.div`
  background-color: #b6f525;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: 5px;
  padding: 5px;
  border-radius: 15px;
`;

function App(): JSX.Element {
  return (
    <Container>
      <Menu
        backgroundColor={"#21d0d0"}
        options={["PRICE LOW TO HIGH", "PRICE HIGH TO LOW", "POPULARITY"]}
        onCheck={(e) => console.log(e)}
      />
      <Menu onCheck={(e) => console.log(e)} />
    </Container>
  );
}

export default App;
