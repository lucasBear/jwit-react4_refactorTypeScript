import styled from "styled-components";
import CheckBox from "./checksBox";
import checkIcon from "../img/checkIcon.png";

const Container = styled.div`
  margin-left: 25px;
  margin-top: 15px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-right: 25px;
  align-items: center;
`;

const Title = styled.p`
  margin: 0px;
  color: white;
  font-weight: 600;
  text-transform: uppercase;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen,
    Ubuntu, Cantarell, "Open Sans", "Helvetica Neue", sans-serif;
`;

export interface params {
  value: string;
  onClick?: (params: { value: string; state: boolean }) => void;
}

function Options(params: params): JSX.Element {
  const handleClick = (state: boolean) => {
    if (typeof params.onClick === "function")
      params.onClick({ value: params.value, state });
  };
  return (
    <Container>
      <Title>{params.value}</Title>
      <CheckBox image={checkIcon} onClick={handleClick} />
    </Container>
  );
}

export default Options;
