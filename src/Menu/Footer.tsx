import styled from "styled-components";
import Button from "./Buttons";
import checkIcon from "../img/checkIcon.png";
import cancelIcon from "../img/cancelIcon.png";
import redoIcon from "../img/redoIcon.png";

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  margin-top: 15px;
  padding: 15px 55px 15px 55px;
  border-top: 1px solid #ffffff;
`;

/* Defining the type of the data array. */
interface option {
  icon: string;
  onClick: () => void;
}

export interface params {
  enableReset?: boolean;
  enableCancel?: boolean;
  onCheck?: () => void;
  onReset?: () => void;
  onCancel?: () => void;
}

function Footer(params: params): JSX.Element {
  const handleCheck = () => {
    if (typeof params.onCheck === "function") params.onCheck();
  };

  const handleReset = () => {
    if (typeof params.onReset === "function") params.onReset();
  };

  const handleCancel = () => {
    if (typeof params.onCancel === "function") params.onCancel();
  };

  const data: option[] = [{ icon: checkIcon, onClick: handleCheck }];

  if (params.enableReset) data.push({ icon: redoIcon, onClick: handleReset });
  if (params.enableCancel)
    data.push({ icon: cancelIcon, onClick: handleCancel });

  return (
    <Container>
      {data.map((v, i) => (
        <Button key={i} image={v.icon} onClick={v.onClick} />
      ))}
    </Container>
  );
}

Footer.defaultProps = {
  enableReset: true,
  enableCancel: true,
};

export default Footer;
