import styled from "styled-components";
import { useState } from "react";

const Container = styled.div<{ visible: boolean }>`
  border-radius: 50%;
  border: 1.8px solid white;
  height: 35px;
  width: 35px;
  justify-content: center;
  align-items: center;
  display: flex;
  cursor: pointer;
  background-color: ${(props) => (props.visible ? "white" : "transparent")};
`;

const Icon = styled.img<{ visible: boolean }>`
  height: 30px;
  width: 30px;
  object-fit: contain;
  border-radius: 50%;
  visibility: ${(props) => (props.visible ? "visible" : "hidden")};
`;

export interface params {
  image: string;
  onClick?: (value: boolean) => void;
}

function checksBox(params: params): JSX.Element {
  const [visible, setVisible] = useState<boolean>(false);

  const handleClick = () => {
    setVisible(!visible);

    if (typeof params.onClick === "function") params.onClick(!visible);
  };
  return (
    <Container visible={visible} onClick={handleClick}>
      <Icon src={params.image} visible={visible} />
    </Container>
  );
}

export default checksBox;
