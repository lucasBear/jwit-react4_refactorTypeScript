import styled from "styled-components";
import Option, { params as paramsComponentOption } from "./Option";
import Footer from "./Footer";
import { useState, useEffect } from "react";

const Container = styled.div`
  background-color: ${(props) => props.color};
  margin-top: 15px;
  padding-top: 25px;
  padding-top: 15px;
  width: 400px;
  border-radius: 25px;
`;

export interface select {
  value: string;
  state: boolean;
}
export interface params {
  options: string[];
  backgroundColor?: string;
  onCheck: (options: select[]) => void;
}

const App = (params: params): JSX.Element => {
  const [options, setOptions] = useState<select[]>([]);

  useEffect(() => {
    var raw: select[] = [];
    params.options.forEach((el) => raw.push({ value: el, state: false }));
    setOptions(raw);
  }, [params.options]);

  const handleClick: paramsComponentOption["onClick"] = (e) => {
    const copy = [...options];
    const index: number = copy.findIndex((f) => f.value === e.value);

    if (index > -1) {
      copy[index] = e;
    }

    setOptions(copy);
    console.log(copy);
  };

  const handleCheck = () => {
    if (typeof params.onCheck === "function") params.onCheck(options);
  };

  const handleReset = () => {
    let raw: select[] = [];
    params.options.forEach((el) => raw.push({ value: el, state: false }));
    setOptions(raw);
  };

  return (
    <Container color={params.backgroundColor}>
      {params.options.map((v, i) => (
        <Option key={i} value={v} onClick={handleClick} />
      ))}

      <Footer onCheck={handleCheck} onReset={handleReset} />
    </Container>
  );
};

App.defaultProps = {
  options: [
    "1UP NUTRITION",
    "ASITIS ",
    "AVVATAR",
    "BIG MUSCLES",
    "BPI SPORTS",
    "BSN",
    "CELLUCOR",
    "DOMINBR",
  ],
  backgroundColor: "#ff7745",
};
export default App;
