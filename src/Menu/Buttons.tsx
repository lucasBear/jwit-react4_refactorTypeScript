import styled from "styled-components";

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 60px;
  width: 60px;
  border-radius: 50%;
  border: 1.8px solid white;
  background-color: white;
  margin-left: 10px;
  box-shadow: 0px 14px 4px -3px rgba(153, 145, 153, 0.26);
  cursor: pointer;
`;

const Icon = styled.img`
  height: 65%;
  width: 100%;
  object-fit: contain;
  border-radius: 50%;
`;

export interface params {
  image: string;
  onClick?: () => void;
}

function checksBox(params: params): JSX.Element {
  const handleClick = () => {
    if (typeof params.onClick === "function") params.onClick();
  };
  return (
    <Container onClick={handleClick}>
      <Icon src={params.image} />
    </Container>
  );
}

export default checksBox;
